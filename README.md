# Hands-On Assigment 2

This assignment involves preparing real-world data for a machine learning task and considering some of the choices we are forced to make in the process.

* Complete the assignment by running `jupyter lab` (or `jupyter notebook`) and editing ho2.ipynb.
* Run `pytest` in this directory to test your edits.
* Add, Commit, and Push your edits to your individual repository to submit the assignment.