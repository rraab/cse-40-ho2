#!/usr/bin/env python3

import pytest
import testbook as tb
import pandas as pd
import json

exe_cells = [
    'q0', 'q1', 'q2a', 'q2b', 'q3a', 'q3b', 'q4', 'q5'
]

@pytest.fixture(scope='module')
def nb():
    with tb.testbook('ho2.ipynb', execute=exe_cells) as nb:
        yield nb

@pytest.mark.filterwarnings('ignore::DeprecationWarning')
def test_q1(nb):

    print('The Function `q1` must return a pandas dataframe.')

    nb.inject(
        "assert isinstance(q1(df, ['Export commodities']), pd.DataFrame)"
    )

@pytest.mark.filterwarnings('ignore::DeprecationWarning')
def test_q2a(nb):

    nb.inject(
        """freq_dict = q2a(df_2a['Export commodities'].apply(get_set_of_words))
for k, v in freq_dict.items():
    assert isinstance(k, str)
    assert np.any([
        isinstance(v, int),
        isinstance(v, np.int64)
    ])
        """
    )


@pytest.mark.filterwarnings('ignore::DeprecationWarning')
def test_q2b(nb):

    nb.inject(
        "assert q2b({1, 2, 3, 4, 5}, 6) in [True, False]"
    )

@pytest.mark.filterwarnings('ignore::DeprecationWarning')
def test_q3a(nb):

    nb.inject(
        "assert isinstance(q3a(df_2b), pd.DataFrame)"
    )

@pytest.mark.filterwarnings('ignore::DeprecationWarning')
def test_q3b(nb):

    nb.inject(
        "assert isinstance(q3b(df_3a), pd.DataFrame)"
    )

@pytest.mark.filterwarnings('ignore::DeprecationWarning')
def test_q4(nb):

    print("q4 must accept a dataframe and a string and return an integer")

    nb.inject(
        """assert np.any([
            isinstance(q4(df_3b, 'Population'), int),
            isinstance(q4(df_3b, 'Population'), np.int64)
        ])"""
    )

@pytest.mark.filterwarnings('ignore::DeprecationWarning')
def test_q5(nb):
    nb.inject(
        "assert isinstance(q5(df_5), pd.DataFrame)"
    )
